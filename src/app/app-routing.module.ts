import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPage } from './landing-page/landing-page.component';
import { TrainerPage } from './trainer-page/trainer-page.component';
import { CataloguePage } from './catalogue-page/catalogue-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing',
  },
  {
    path: 'landing',
    component: LandingPage,
  },

  {
    path: 'trainer',
    component: TrainerPage,
  },
  {
    path: 'catalogue',
    pathMatch: 'full',
    component: CataloguePage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
