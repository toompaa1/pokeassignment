import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css'],
})

/*
Trainer page:
setting catched pokemons to an array, retrieving from local storage.
*/
export class TrainerPage {
  constructor(private router: Router) {}
  public catchedPokemons: Array<any> = [];

  ngOnInit() {
    if (!localStorage.hasOwnProperty('User')) {
      this.router.navigateByUrl('/landing');
    } else {
      this.catchedPokemons = JSON.parse(localStorage.getItem('Pokemons')!);
    }
  }
}
