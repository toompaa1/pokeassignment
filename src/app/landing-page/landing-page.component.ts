import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})

/*
Landing page:
is the start page for the application.
functions:
ngOnInit(): check if there already is a user logged in. 
handleUserName(): update the user with the input.
handleLoginButton(): store userName in local storage and redirect to catalogue page. 
*/
export class LandingPage {
  constructor(private router: Router) {}
  public user: string = '';

  ngOnInit() {
    if (localStorage.hasOwnProperty('User')) {
      this.router.navigateByUrl('/catalogue');
    }
  }

  handleUserName(event: any) {
    this.user = event.target.value;
  }

  handleLoginButton(event: any) {
    if (this.user === '') {
      alert('Type in valid name');
    } else {
      localStorage.setItem('User', this.user);
      this.router.navigateByUrl('/catalogue');
    }
  }
}
