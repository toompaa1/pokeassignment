export default interface IPokemon {
  name?: String;
  pokemonImage?: String;
}
