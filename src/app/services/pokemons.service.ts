import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import IPokemon from '../models/pokemon.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PokemonsService {
  //private pokemons: IPokemon[] = [];
  private error: string = '';
  private url: string = 'https://pokeapi.co/api/v2/pokemon?limit=718';
  private tempPokemonArray: Array<any> = [];

  constructor(private readonly http: HttpClient) {}

  public fetchAllPokemons() {
    return this.http.get<IPokemon[]>(this.url);
    //this.pokemons = response;
    // console.log(response);
    // this.tempPokemonArray.push(response);
    //});
  }

  // public Pokemons(): IPokemon[] {
  //   this.fetchAllPokemons();

  //   let pokemonObj = {
  //     name: '',
  //     image: '',
  //   };
  //   console.log(this.getRandomId());
  //   console.log(this.tempPokemonArray);
  //   console.log(this.tempPokemonArray[0].count);

  //   for (let i = 1; i <= 20; i++) {
  //     console.log(this.tempPokemonArray);
  //     pokemonObj.name = this.tempPokemonArray[0].results[1].name;
  //   }

  //   return this.pokemons;
  // }
}
