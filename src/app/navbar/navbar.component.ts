import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class Navbar {
  constructor(private router: Router) {}

  handleLogOut(event: any) {
    localStorage.clear();
    this.router.navigateByUrl('/landing');
  }
}
