import { Component, OnInit } from '@angular/core';
import { PokemonsService } from '../services/pokemons.service';
import IPokemon from '../models/pokemon.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css'],
})

/*
CataloguePage
Constructor: defines pokemon service that fetch pokemons and defines the router.
Functions: 
getRandomId(): generates a random integer.
handleImageCliked(): a click event. When a pokmon is clicked the pokemon is added to the 
catchedPokemon array and pushed to local storage. 
getAllPokemons(): gets all pokemons by subscribing on the pokemon service, then randomly 
select 63 pokemons and store them in the pokemons array. 
ngOnInit(): when the class is loaded, check that a user is logged in and fetch pokemons.
*/
export class CataloguePage implements OnInit {
  constructor(
    private readonly pokemonService: PokemonsService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  public pokemons: IPokemon[] = [];
  public pokemonsHolder: any = {};
  public catchedPokemons: Array<any> = [];

  ngOnInit(): void {
    if (!localStorage.hasOwnProperty('User')) {
      this.router.navigateByUrl('/landing');
    }
    this.getAllPokemons();
  }

  public getRandomId(): number {
    return Math.floor(Math.random() * 718);
  }

  public handleImageClicked(myPokemon: any) {
    if (!localStorage.hasOwnProperty('Pokemons')) {
      this.catchedPokemons.push(myPokemon);
      localStorage.setItem('Pokemons', JSON.stringify(this.catchedPokemons));
    } else {
      this.catchedPokemons = JSON.parse(localStorage.getItem('Pokemons')!);
      this.catchedPokemons.push(myPokemon);
      localStorage.setItem('Pokemons', JSON.stringify(this.catchedPokemons));
      this.toastr.success(myPokemon.name.toUpperCase(), 'YOU CATCHED!');
    }
  }

  private getAllPokemons() {
    this.pokemonService.fetchAllPokemons().subscribe(
      (pokemon: any) => {
        this.pokemonsHolder = pokemon.results;
        for (let i = 0; i <= 62; i++) {
          const id = this.getRandomId();
          const pokemon = {
            name: this.pokemonsHolder[id].name,
            pokemonImage: `/assets/images/${id + 1}.png`,
          };
          this.pokemons.push(pokemon);
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  }
}
